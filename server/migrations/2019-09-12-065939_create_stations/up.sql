CREATE TABLE stations (
  id UUID NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4 (),
  label VARCHAR NOT NULL,
  latitude FLOAT NOT NULL,
  longitude FLOAT NOT NULL
);

CREATE TABLE sensor_types (
  id SERIAL PRIMARY KEY,
  label VARCHAR NOT NULL,
  description VARCHAR NOT NULL
);

CREATE TABLE sensors (
  id UUID NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
  alias VARCHAR NOT NULL,
  label VARCHAR NOT NULL,
  type_id INT NOT NULL,
  station_id UUID NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT NOW(),
  updated_at TIMESTAMP NOT NULL DEFAULT NOW(),
  FOREIGN KEY (type_id) REFERENCES sensor_types(id),
  FOREIGN KEY (station_id) REFERENCES stations(id)
);

CREATE TABLE measurements (
  id UUID PRIMARY KEY DEFAULT uuid_generate_v4 (),
  value VARCHAR NOT NULL,
  sensor_id UUID NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT NOW(),
  FOREIGN KEY (sensor_id) REFERENCES sensors(id)
);

INSERT INTO sensor_types(id, label, description) VALUES (1, 'celsius', 'celsius');
INSERT INTO sensor_types(id, label, description) VALUES (2, 'humidity', 'relative humitidy');
INSERT INTO sensor_types(id, label, description) VALUES (3, 'hpa', 'hectopascal');
INSERT INTO sensor_types(id, label, description) VALUES (4, 'meters', 'meters above sea level');
INSERT INTO sensor_types(id, label, description) VALUES (5, 'dbm', 'decibels relative to a milliwatt');
INSERT INTO sensor_types(id, label, description) VALUES (6, 'voltage', '18650 voltage');

INSERT INTO stations(id, label, latitude, longitude) VALUES ('eb54ffd7-34fb-4599-a5df-efdd2b8bf46c', 'Hortonia, VT', 43.74721, -73.21261);
INSERT INTO sensors(alias, label, type_id, station_id) VALUES ('air_temp', 'Air temperature', 1, 'eb54ffd7-34fb-4599-a5df-efdd2b8bf46c');
INSERT INTO sensors(alias, label, type_id, station_id) VALUES ('water_temp', 'Water temperature (3ft)', 1, 'eb54ffd7-34fb-4599-a5df-efdd2b8bf46c');
INSERT INTO sensors(alias, label, type_id, station_id) VALUES ('humidity', 'Humidity', 2, 'eb54ffd7-34fb-4599-a5df-efdd2b8bf46c');
INSERT INTO sensors(alias, label, type_id, station_id) VALUES ('pressure', 'Pressure', 3, 'eb54ffd7-34fb-4599-a5df-efdd2b8bf46c');
INSERT INTO sensors(alias, label, type_id, station_id) VALUES ('altitude', 'Altitude', 4, 'eb54ffd7-34fb-4599-a5df-efdd2b8bf46c');
INSERT INTO sensors(alias, label, type_id, station_id) VALUES ('signal', 'Signal strength', 5, 'eb54ffd7-34fb-4599-a5df-efdd2b8bf46c');
INSERT INTO sensors(alias, label, type_id, station_id) VALUES ('voltage', 'Voltage', 6, 'eb54ffd7-34fb-4599-a5df-efdd2b8bf46c');

INSERT INTO stations(id, label, latitude, longitude) VALUES ('f9f32d13-d08e-49a0-b07f-a80cd98d27fe', 'Swift Creek, VA', 37.42194, -77.64615);
INSERT INTO sensors(alias, label, type_id, station_id) VALUES ('temp', 'Air temperature', 1, 'f9f32d13-d08e-49a0-b07f-a80cd98d27fe');
INSERT INTO sensors(alias, label, type_id, station_id) VALUES ('humidity', 'Humidity', 2, 'f9f32d13-d08e-49a0-b07f-a80cd98d27fe');
INSERT INTO sensors(alias, label, type_id, station_id) VALUES ('pressure', 'Pressure', 3, 'f9f32d13-d08e-49a0-b07f-a80cd98d27fe');
INSERT INTO sensors(alias, label, type_id, station_id) VALUES ('altitude', 'Altitude', 4, 'f9f32d13-d08e-49a0-b07f-a80cd98d27fe');
INSERT INTO sensors(alias, label, type_id, station_id) VALUES ('signal', 'Signal strength', 5, 'f9f32d13-d08e-49a0-b07f-a80cd98d27fe');
INSERT INTO sensors(alias, label, type_id, station_id) VALUES ('voltage', 'Voltage', 6, 'f9f32d13-d08e-49a0-b07f-a80cd98d27fe');
