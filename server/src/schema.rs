
table! {
    measurements (id) {
        id -> Uuid,
        value -> Varchar,
        sensor_id -> Uuid,
    }
}

table! {
    sensor_types (id) {
        id -> Int4,
        label -> Varchar,
        description -> Varchar,
    }
}

table! {
    sensors (id) {
        id -> Uuid,
        alias -> Varchar,
        label -> Varchar,
        type_id -> Int4,
        station_id -> Uuid,
    }
}

table! {
    stations (id) {
        id -> Uuid,
        label -> Varchar,
        latitude -> Float8,
        longitude -> Float8,
    }
}

joinable!(measurements -> sensors (sensor_id));
joinable!(sensors -> sensor_types (type_id));
joinable!(sensors -> stations (station_id));

allow_tables_to_appear_in_same_query!(
    measurements,
    sensor_types,
    sensors,
    stations,
);
