#[macro_use]
extern crate diesel;

use std::{
    net::UdpSocket,
    collections::HashMap,
};

use log::{trace, warn};

use nom;
use nom::sequence::separated_pair;
use nom::bytes::complete::tag;
use nom::branch::alt;
use nom::number::complete::double;
use nom::character::complete::anychar;
use nom::bytes::complete::take_till;
use nom::combinator::{map, opt};
use thiserror::Error;

use diesel::prelude::*;

pub mod schema;
pub mod models;

#[derive(Error, Debug, Clone)]
pub enum MessageParseError {
    #[error("message was not formatted correctly")]
    FormatError,
    #[error("missing field {0} in the message")]
    MissingField(String),
}

impl<I> From<nom::Err<(I, nom::error::ErrorKind)>> for MessageParseError {
    fn from(_: nom::Err<(I, nom::error::ErrorKind)>) -> Self {
        MessageParseError::FormatError
    }
}

#[derive(Debug, Clone)]
pub struct Message {
    pub measures: HashMap<String, f64>,
}

impl Default for Message {
    fn default() -> Self {
        Message {
            measures: HashMap::new(),
        }
    }
}

/// Parses a slice with the form "key=value", seperated by commas. Expects only
/// valid fields; pressure, air_temp, voltage, signal, id.
fn parse_message(mut input: &[u8]) -> Result<(&[u8], Message), MessageParseError> {
    let mut values: HashMap<String, f64> = HashMap::new();
    while input.len() != 0 {
        trace!("parsing next measure; remaining: {}", String::from_utf8_lossy(input));
        let (rest, (name, value)) = separated_pair(take_till(|b| b == b'=' ), tag("="), double)(input)?;
        let (rest, _) = opt(tag(","))(rest)?;
        input = rest;
        // TODO: consider putting a check here to ensure that the name is valid;
        // there might be potentional for a DOS attack here since we insert it
        // to a map, which isn't free. As an alternative, use hard-coded enums
        // rather than a map
        values.insert(String::from_utf8_lossy(name).to_string(), value);
    }
    Ok((input, Message{
        measures: values,
    }))
}

fn perform_crc_check(_input: &[u8], _expected_checksum: usize) -> Result<(), MessageParseError> {
    // TODO: I didn't implement this because I'm not clear on how you're doing it;
    // it looks like your just doing a s/value of id/some key/g then running it through
    // crc32... This doesn't seem right. I think it would be best to actually move
    // the checksum into a clearly specified location (maybe the last N bytes)
    Ok(())
}

pub struct UdpServer {
    buff: Vec<u8>,
    sock: UdpSocket,
}

impl UdpServer {
    /// constructs a new server which will run on the given address
    pub fn new(sock: UdpSocket) -> Self {
        UdpServer {
            // max udp packet length
            buff: vec![0; 65535],
            sock,
        }
    }
}

impl Iterator for UdpServer {
    type Item = Message;

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            let read_result = self.sock.recv(&mut self.buff);
            trace!("read message {:?}", read_result);
            if read_result.is_err() {
                warn!("error reading data from socket; {:?}", read_result);
                return None;
            }
            let count = read_result.unwrap();
            let slice = &self.buff[0..count];
            // parse the message
            let msg = parse_message(slice);
            if msg.is_err() {
                warn!("dropping invalid message; error was {:?}", msg);
                continue;
            }
            let msg = msg.unwrap();
            let id = match msg.1.measures.get("id") {
                Some(id) => id,
                None => {
                    warn!("dropping invalid message; message missing field id");
                    continue;
                },
            };
            if let Err(_) = perform_crc_check(slice, *id as usize) {
                warn!("dropping invalid message; CRC checksum failed");
                continue;
            }
            let msg = msg.1;
            return Some(msg);
        }
    }
}

#[cfg(test)]
mod tests {
    use std::thread;
    use super::*;

    const VALID_MESSAGE: &[u8] = b"pressure=987.89,air_temp=25.46,voltage=3.93,signal=-42,id=2705904064";

    fn create_udp_server() -> Result<(UdpSocket, UdpServer), Box<dyn std::error::Error>> {
        let server_socket = UdpSocket::bind(":::0")?;
        let client_socket = UdpSocket::bind(":::0")?;
        client_socket.connect(server_socket.local_addr()?);
        Ok((client_socket, UdpServer::new(server_socket)))
    }

    #[test]
    fn test_iterate_some_messages() {
        let (client, server) = create_udp_server().unwrap();
        thread::spawn(move || {
            client.send(VALID_MESSAGE).unwrap();
        });
        let mut count = 0;
        for msg in server {
            count += 1;
            break;
        }
        assert_eq!(1, count);
    }

    #[test]
    fn test_parse_message() {
        let (rest, result) = parse_message(VALID_MESSAGE).unwrap();
        assert_eq!(rest.len(), 0);
        // try a weirdly formed message
        let res = parse_message(b"hello my name is bob");
        assert_eq!(true, res.is_err());
        // try a missing field
        let res = parse_message(b"pressure=1000");
        match res.clone().err().unwrap() {
            MessageParseError::MissingField(_) => (),
            _ => panic!("wrong error type! got {:?}", res),
        }
    }
}
