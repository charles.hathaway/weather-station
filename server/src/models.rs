use std::{
    error::Error,
    sync::{Arc, Mutex},
};
use crate::Message;
use crate::schema::measurements;

use diesel::prelude::*;
use diesel::connection::Connection;
use diesel::pg::PgConnection;
use uuid::Uuid;

use log::{info};
type Database = Arc<Mutex<PgConnection>>;

#[derive(Queryable, Insertable)]
#[table_name="measurements"]
struct Measurement {
    id: Uuid,
    value: String,
    sensor_id: Uuid,
}

#[derive(Queryable)]
struct SensorType {
    id: i32,
    label: String,
    description: String,
}

#[derive(Queryable)]
struct Sensor {
    id: Uuid,
    alias: String,
    label: String,
    type_id: i32,
    station_id: Uuid,
}

pub fn insert_measurement(db: &Database, msg: &Message, station_id: Uuid) -> Result<(), Box<dyn Error>> {
    use crate::schema::sensors::dsl::*;
    use crate::schema::sensor_types::dsl::*;
    let db = db.lock().unwrap();
    // lookup the sensors associated with this station
    let station_sensors = sensors.filter(station_id.eq(station_id))
        .inner_join(sensor_types)
        .load::<(Sensor, SensorType)>(&*db)?;
    // start building out our insertions
    let mut new_values = vec!();
    for (sensor, sensor_type) in station_sensors {
        let value = msg.measures.get(&sensor_type.label);
        if value.is_none() {
            info!("skipping sensor type {}; no measurement for it", sensor_type.label);
            continue;
        }
        let value = value.unwrap();
        let measure = Measurement {
            id: Uuid::new_v4(),
            sensor_id: sensor.id,
            value: value.to_string(),
        };
        new_values.push(measure);
    }
    diesel::insert_into(measurements::table)
        .values(&new_values)
        .get_results::<Measurement>(&*db)?;
    Ok(())
}
