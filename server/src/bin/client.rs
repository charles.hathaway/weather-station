use std::{
    net::UdpSocket,
};

fn main() -> std::io::Result<()> {
    let sock = UdpSocket::bind(":::0")?;
    sock.connect("127.0.0.1:1337")?;
    sock.send(b"pressure=987.89,air_temp=25.46,voltage=3.93,signal=-42,id=2705904064")?;
    Ok(())
}
