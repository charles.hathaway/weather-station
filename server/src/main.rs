use std::{
    io,
    net::UdpSocket,
    sync::{Arc, Mutex},
};
use uuid::Uuid;
use env_logger::Env;
use log::{info, trace, warn};
use weather_station_backend::{UdpServer, models};

use diesel::prelude::*;
use diesel::pg::PgConnection;
use dotenv::dotenv;
use std::env;

type Database = Arc<Mutex<PgConnection>>;

const STATION_ID: &str = "eb54ffd7-34fb-4599-a5df-efdd2b8bf46c";

/// runs an infinite loop to process UDP messages
fn udp_server(server: UdpServer, db: Database) {
    let station_id = Uuid::parse_str(STATION_ID).unwrap();
    // run the UDP server
    for msg in server {
        info!("Got message {:?}", msg);
        if let Err(e) = models::insert_measurement(&db, &msg, station_id) {
            warn!("error writing measurement to database: {:?}", e);
        } else {
            trace!("wrote message ({:?}) to database", msg);
        }
    }
}

fn main() -> io::Result<()> {
    env_logger::from_env(Env::default().default_filter_or("trace")).init();
    dotenv().ok();
    let database_url = env::var("DATABASE_URL")
        .expect("DATABASE_URL must be set");
    trace!("connecting to database {:?}", database_url);
    let db = PgConnection::establish(&database_url)
        .expect(&format!("Error connecting to {}", database_url));
    let db = Arc::new(Mutex::new(db));

    let sock = UdpSocket::bind("0.0.0.0:1337")?;
    info!("Server started on {:?}; waiting for messages", sock.local_addr()?);
    let server = UdpServer::new(sock);
    udp_server(server, db.clone());
    Ok(())
}
