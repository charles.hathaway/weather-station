# Weather station progressive web app

- [Getting started](#getting-started)
- [Deploy to production](#deploy-to-production)
- [Testing](#testing)
- [Customize the configuration](#customize-the-configuration)

## Getting started

1. Install [yarn](https://yarnpkg.com/en/).
2. Install node modules:
```bash
yarn install
```
3. Copy "secrets.json.example" to "secrets.json".
4. Update the OpenStreetMap URL and api key in the json file to match the service of your choice such as [ThunderForest](https://www.thunderforest.com), [MapBox](https://www.mapbox.com/), or [Stamen](http://maps.stamen.com/). If you don't provide an API key the app will still work but maps won't.
5. Optionally start the backend graphql server. All the mock data runs in memory so there is no need to set up a database:
```bash
yarn apollo
```
6. Start the frontend webpack server:
```bash
yarn start
```

## Deploy to production

The frontend assets can be compiled and minified using the following command:
```bash
yarn build
```

This will generate a "dist/" folder that can then be published.
Any API keys you plan to use in production should be whitelisted for the specific domain you are using to prevent hijacking.

## Testing

Currently the only testing in place is a linter, which can be ran with the following command:
```bash
yarn lint
```

## Customize the configuration

See the official [configuration reference](https://cli.vuejs.org/config/).
