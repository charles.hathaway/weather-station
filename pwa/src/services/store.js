import Vue from 'vue'
import Vuex from 'vuex'
import { debounce } from 'lodash-es'

// This gets bumped whenever a breaking change
// is made to the localStorage data structure.
const schemaVersion = 0

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    darkTheme: false,
    drawerOpen: false,
    pageTitle: ''
  },
  mutations: {
    setDarkTheme(state, boolean) {
      state.darkTheme = boolean
    },
    setDrawer(state, boolean) {
      state.drawerOpen = boolean
    },
    toggleDrawer(state, boolean) {
      state.drawerOpen = !state.drawerOpen
    },
    setPageTitle(state, string) {
      state.pageTitle = string
    }
  },
  plugins: [localStorageSync],
  actions: {

  }
})

export default store

function localStorageSync(store) { // eslint-disable-line no-shadow
  // Restore app store from offline storage (if the schema matches
  const previousState = JSON.parse(localStorage.getItem('appState') || '{}')
  if (previousState.version === schemaVersion) {
    store.replaceState(JSON.parse(previousState))
  }

  store.subscribe(debounce((mutation, state) => {
    localStorage.setItem('appState', JSON.stringify(state))
  }), 200)
}
