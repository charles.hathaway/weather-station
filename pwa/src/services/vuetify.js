import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import { light } from './themes'

Vue.use(Vuetify)

export default new Vuetify({
  theme: {
    themes: {
      light
    }
  },
  icons: {
    iconfont: 'mdiSvg' // 'mdi' || 'mdiSvg' || 'md' || 'fa' || 'fa4'
  }
})
