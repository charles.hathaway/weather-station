// Color themes for vuetify, apexcharts, etc.
// TODO: add a dark theme

// Work-in-progress palette: http://paletton.com/#uid=53A0u0kllllaFw0g0qFqFg0w0aF
export const light = {
  primary: '#373276', // purple
  secondary: '#267158',
  //secondary: '#2b4c6f',
  //secondary: '#64B5F6',
  accent: '#aa9039',
  error: '#e64a19',
  warning: '#ffeb3b',
  info: '#267158',
  success: '#1976d2'
}
