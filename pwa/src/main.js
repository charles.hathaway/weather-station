import Vue from 'vue'
import layoutComponent from './components/layout'
import router from './services/router'
import './services/service-worker'
import vuetify from './services/vuetify'
import { createProvider } from './vue-apollo'
import store from './services/store'

Vue.config.productionTip = false

new Vue({
  router,
  vuetify,
  apolloProvider: createProvider(),
  store,
  render: renderFn => renderFn(layoutComponent)
}).$mount('#app')
