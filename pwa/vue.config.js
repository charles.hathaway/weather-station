const webpack = require('webpack')
const secrets = require('./secrets.json')

module.exports = {
  configureWebpack: {
    plugins: [
      new webpack.DefinePlugin({
        WEBPACK_MAP_URL: JSON.stringify(secrets.mapUrl)
      })
    ]
  },
  pluginOptions: {
    apollo: {
      enableMocks: false,
      enableEngine: false
    }
  }
}
