export const locations = [
  {
    id: 'b7b61603-8693-407e-bdd0-b222dc6e0e0b',
    label: 'Lake Hortonia, Vermont',
    latitude: 43.74721,
    longitude: -73.21261,
    sensors: () => [
      sensors[0],
      sensors[1],
      sensors[2]
    ]
  },
  {
    id: '00b596d9-b500-4699-bc55-245d3daaeb17',
    label: 'Swiftcreek Reservoir, Virginia',
    latitude: 37.42194,
    longitude: -77.64615,
    sensors: () => [
      sensors[3]
    ]
  }
]

export const sensors = [
  {
    id: 'fbad4901-5ff0-4e28-878e-ac149389d3a5',
    label: 'Lake temp (3ft)',
    type: 'TEMPERATURE',
    location: () => locations[0]
  },
  {
    id: '08fc84fa-a9d5-4834-ba80-bd2b0f7a5c51',
    label: 'Temperature',
    type: 'TEMPERATURE',
    location: () => locations[0]
  },
  {
    id: 'cf809be2-e627-4d0f-b2f8-6028da097ea3',
    label: 'Humidity',
    type: 'HUMIDITY',
    location: () => locations[0]
  },
  {
    id: 'c1c9a847-9ead-4d37-acf3-1cfd00d43b04',
    label: 'Humidity',
    type: 'HUMIDITY',
    location: () => locations[1]
  }
]
