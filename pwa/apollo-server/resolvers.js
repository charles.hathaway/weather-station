import GraphQLJSON from 'graphql-type-json'
import { locations, sensors } from './db'

export default {
  JSON: GraphQLJSON,

  Query: {
    location: (root, { id }) => locations.find(el => el.id === id) || locations[0],
    locations: () => locations,
    sensor: (root, { id }) => sensors.find(el => el.id === id) || sensors[0],
    sensors: () => sensors
  },

  Mutation: {
    myMutation: (root, args, context) => {
      const message = 'My mutation completed!'
      context.pubsub.publish('hey', { mySub: message })
      return message
    }
  },

  Subscription: {
    mySub: {
      subscribe: (parent, args, { pubsub }) => pubsub.asyncIterator('hey')
    }
  }
}
