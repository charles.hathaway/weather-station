void sleep() {
  // calculate required sleep time and go to sleep
  long sleepTime = measurementInterval - millis(); // in milliseconds
  if (sleepTime < 100) sleepTime = 100; // set minimum sleep of 0.1 second

  Serial.print ("Going to sleep now for ");
  Serial.print((float)sleepTime / 1000, 3);
  Serial.println (" seconds.");
  Serial.print ("Time going to sleep=");
  Serial.print ((float)millis() / 1000, 3);
  //Serial.print (" Event elapsed Time=");
  //Serial.println((float)(millis()-baseEventTime)/1000,3);
  Serial.println();
  ESP.deepSleep(sleepTime * 5000); // convert to microseconds
}
