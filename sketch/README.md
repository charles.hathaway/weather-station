# ESP8266 sketch

- [Setting up the Arduino IDE](#setting-up-arduino-ide)
- [Configuration](#configuration)

## Setting up the Arduino IDE

The most common way to get the software compiled and uploaded to the ESP8266 board is to set up the [Arduino IDE](https://www.arduino.cc/en/Main/Software).

- Install the Arduino IDE or if you prefer to use the web-based version, follow the link to create an account
- Under "File" > "Preferences", there is a section that says "Additional Boards Manager URLs". Paste in the URL for the [ESP8266 library](https://github.com/esp8266/Arduino):

```
https://arduino.esp8266.com/stable/package_esp8266com_index.json
```

- Download the [BMP280 sensor's library](https://github.com/farmerkeith/BMP280-library/archive/master.zip)
- Under "Sketch" > "Include Library" > "Add .ZIP Library", navigate to the downloaded zip file and select it to add it to the IDE.
- Under library manager, install the "Arduino_CRC32", "MAX31850 OneWire" and "MAX31850 DallasTemp" libraries
- Under "Tools" > "Board", select "WeMos D1 R1". This will configure the hardware parameters for you. Otherwise, ensure these values match:
  - Upload Speed: 921600 [bps]
  - CPU Frequency: 80MHz
  - Flash Size: 4MB (FS:2MB OTA:~1019KB)
  - IwIP Variant: v2 Lower Memory
- Plug in your device via USB if you haven't already
- Under "Tools" > "Port", select the port for your ESP8266
  - If the port option is disabled and you are on Windows, ensure you have the correct drivers installed
  - If the port option is disabled on Linux, ensure your user has the correct permissions:
```
# See what group owns the device
ls -lh /dev/ttyUSB0

# You'll see something like "tty", "dialout", or "uucp" for example:
#  crw-rw---- 1 root uucp 188, 0 Jun 24 19:55 /dev/ttyUSB0

# Add your user to that group then reboot your machine
sudo usermod -a -G uucp yourUserName
```
- The program will still fail to compile until the config file is created (see below)

## Configuration

- Copy config.h.example to config.h and adjust the variables as needed.
