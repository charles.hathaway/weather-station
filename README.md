ESP8266 sketch and frontend web app. For installation and setup, see the individual README files.

- [Overview](#overview)
- [pwa README](pwa/README.md)
- [server README](server/README.md)
- [sketch README](sketch/README.md)

## Overview

This a monorepo for all the software required to run the solar-powered wi-fi weather station [on instructables.com](https://www.instructables.com/id/Solar-Powered-WiFi-Weather-Station-V20/).
This includes the web/mobile app and backend server for recording sensor data.
For building the weather station, see the instructable link.

## Deploying

Follow the [server README](server/README.md) for deploying the server to a host machine and configuring your first weather station.
Afterwards, follow the [sketch README](sketch/README.md) for uploading the software to your weather station's esp8266 system-on-a-chip.
